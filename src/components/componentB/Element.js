import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 90%;
`;

const Body = styled.p`
  font-size: 15px;
`;

function Element({ title }) {
  return (
    <Container>
      <Body>{title}</Body>
    </Container>
  );
}

export default Element;

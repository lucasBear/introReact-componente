import styled from "styled-components";

const Container = styled.div`
  width: 10%;
`;

const Icon = styled.img`
  height: 15px;
  width: 15px;
`;

function App({ icon }) {
  return (
    <Container>
      <Icon src={icon} />
    </Container>
  );
}

export default App;

import cartIcon from './img/cart.png';
import statsIcon from './img/stats.png';
import clientIcon from './img/clients.png';
import discountIcon from './img/percent.png';
import formIcon from './img/form.png';

export const bodyData = [
  {
    title: "Paquete Premium",
    info: "Descubre nuevas funciones",
    link: "Hazte Premium",
  },
  {
    title: "Dominio",
    info: "Registra tu propio dominio, p. ej. www.mipaginaweb.com",
    link: "Registrar un nuevo dominio",
  },
  {
    title: "Cuentas de email",
    info: "Envia correos profesionales",
    link: "Crear una nueva cuenta de email",
  },
];

export const resumeData = [
  {
    icon: cartIcon,
    title: "Pedidos",
  },
  {
    icon: statsIcon,
    title: "Analítica de negocio",
  },
  {
    icon: clientIcon,
    title: "Clientes",
  },
  {
    icon: discountIcon,
    title: "Descuentos",
  },
  {
    icon: formIcon,
    title: "Formularios recibidos",
  },
];

import styled from "styled-components";
import ComponentA from "./components/componentA";
import ComponentB from "./components/componentB";
import ComponentC from "./components/componentC";
import { bodyData, resumeData } from "./bodyData";
import logo from "./img/logo.png";
import birdImg from "./img/bird.png";
import likeImg from "./img/like.png";

const ContainerMain = styled.div``;

const ContainerA = styled.div``;

const ContainerB = styled.div``;

const ContainerC = styled.div``;

function App() {
  return (
    <ContainerMain>
      <ContainerA>
        <ComponentA />
      </ContainerA>
      <ContainerB>
        {resumeData.map((resume) => {
          return <ComponentB icon={resume.icon} title={resume.title} />;
        })}
      </ContainerB>
      <ContainerC>
        <ComponentC
          icon={logo}
          colorIcon={"#fff6e5"}
          title={bodyData[0].title}
          info={bodyData[0].info}
          link={bodyData[0].link}
        />
        <ComponentC
          icon={birdImg}
          colorIcon={"#ebfcf1"}
          title={bodyData[1].title}
          info={bodyData[1].info}
          link={bodyData[1].link}
        />
        <ComponentC
          icon={likeImg}
          colorIcon={"#f4f2ff"}
          title={bodyData[2].title}
          info={bodyData[2].info}
          link={bodyData[2].link}
        />
      </ContainerC>
    </ContainerMain>
  );
}

export default App;

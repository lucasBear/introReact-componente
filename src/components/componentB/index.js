import styled from "styled-components";
import Element from "./Element";
import Icon from "./Icon";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 500px;
  height: 80px;
`;

function App(resume) {
  return (
    <Container>
      <Icon icon={resume.icon} />
      <Element title={resume.title} />
    </Container>
  );
}

export default App;

import styled from "styled-components";
import Body from "./Body";
import Icon from "./Icon";

const Container = styled.div`
  height: 80px;
  width: 350px;
  display: flex;
  flex-direction: row;
  margin-top: 15px;
`;

function App(params) {
  return (
    <Container>
      <Icon icon={params.icon} colorIcon={params.colorIcon} />
      <Body title={params.title} info={params.info} link={params.link} />
    </Container>
  );
}

export default App;

import styled from "styled-components";

const Container = styled.div`
  width: 75%;
  padding-left: 25px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const Title = styled.p`
  margin: 0px;
  color: #405382;
  font-weight: bold;
  font-size: 15px;
`;

const Description = styled.p`
  margin: 0px;
  margin-top: 5px;
  color: #828b92;
  font-size: 14px;
`;

const Link = styled.a`
  margin: 0px;
  margin-top: 5px;
  color: #3b8699;
  font-size: 14px;
  cursor: pointer;
`;

function App({ title, info, link }) {
  return (
    <Container>
      <Title>{title}</Title>
      <Description>{info}</Description>
      <Link>{link}</Link>
    </Container>
  );
}

export default App;

import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 50%;
  justify-content: space-between;
`;

const Option = styled.p`
  font-size: 20px;
`;

function App() {
  return (
    <Container>
      <Option>Resumen</Option>
      <Option>Administración</Option>
      <Option>Configuración de tienda online</Option>
    </Container>
  );
}

export default App;
